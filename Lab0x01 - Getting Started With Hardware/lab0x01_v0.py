# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 09:14:17 2022

@author: melab15
"""
#execfile('lab0x01.py')
import pyb
import time
import math

pinA5 = pyb.Pin (pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

def onButtonPressFCN(IRQ_src):
    global buttonPressed
    buttonPressed = True
    print("button pressed")
        
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

def SquareWave(t):
    remainder = t%1
    if remainder <.50:
        return 100
    else:
        return 0

def SineWave(t):
    return 100*(0.5+0.5*math.sin(2*3.1415*t/10))

def SawWave(t):
    return 100*(t%1)


if __name__ == '__main__':
    
    buttonPressed = False
    state = 0
    start = time.ticks_ms()
    
    while True:
        try:
            s_time = (time.ticks_ms()-start)/1000
            if s_time > 10:
                start = time.ticks_ms()
            if state == 0:
                if buttonPressed:
                    buttonPressed = False
                    print("in state 1")
                    state = 1
                    start = time.ticks_ms()
            elif state == 1:
                brt = SquareWave(s_time)
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    print("in state 2")
                    state = 2
                    start = time.ticks_ms()
            elif state == 2:
                brt = SineWave(s_time)
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    print("in state 3")
                    state = 3
                    start = time.ticks_ms()
            elif state == 3:
                brt = SawWave(s_time)
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    print("in state 1")
                    state = 1
                    start = time.ticks_ms()
        except KeyboardInterrupt:
            break
    print("Program Terminated")
                

''' 
Pseudocode;
 3 functions besides button press
 - one for each waveform
 - e.g. def SawWave
 - input is t in the waveform, within the period, 
 -output is brightness (duty cycle)
 
 -in main -> run prompt to user, etc.
 - will need a while loop 
 - allow program to be quit wih Ctrl+C
 - Use keyboardinterrupt so other errors are not caught without sending an error message
 - use "buttonpressed variable" so that button dos no directly change state variable
 '''