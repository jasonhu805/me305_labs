# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 09:12:04 2022

@author: melab15
"""


import pyb
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
def onButtonPressFCN(IRQ_src):
    print('Stop pushing my buttons!')
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)