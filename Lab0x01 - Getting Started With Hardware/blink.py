# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 08:38:34 2022

@author: melab15
"""

import pyb
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
t2ch1.pulse_width_percent(10)
pyb.delay(7000)
t2ch1.pulse_width_percent(75)
#t2ch1.pulse_width_percent(100)
