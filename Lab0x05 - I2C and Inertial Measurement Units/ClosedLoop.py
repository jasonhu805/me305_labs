'''!@file       ClosedLoop.py
    @brief      Object which implements closed loop control based on inputs
    @details    Implements an interface that takes in a value for proportional gain,
                velocity setpoint, and measured velocity, and returns an actuation signal,
                in the form of a duty cycle, based on a basic proportional control system.
'''

class ClosedLoop:
    
    
    def __init__(self):
        ## @brief Proportional gain implemented for closed-loop control
        self.kp = 0
        ## @brief Minimum percentage value for the duty cycle
        self.duty_min = -100
        ## @brief Maximum percentage value for the duty cycle
        self.duty_max = 100
        
        
    def run(self, kp_val, desired_vel, measured_vel):
        self.set_kp(kp_val)
        ## @brief The duty cycle calculated based on measured velocity, velocity setpoint, and proportional gain
        self.DC = self.kp*(desired_vel - measured_vel)
        ## Set the motors duty cycle to this value
        if self.DC > self.duty_max:
            return self.duty_max
        if self.DC < self.duty_min:
            return self.duty_min
        return self.DC
        
    def set_kp(self, kp_value):
        self.kp = kp_value