'''!@file       Lab0x05.py
    @brief      A page providing documentation, plots and supporting material for ME 305 Lab0x05

    @page  lab0x05 ME 305 Lab0x05
 
    @section Lab0x05 Lab0x05: I2C and Inertial Measurement Units
    
    @subsection desc Description:
    
    This lab utilizes the Bosch BNO055 inertial measurement unit mounted on the platform. This measurement chip uses I2C to communicate with the 
    STM32 Nucleo microcontroller. The IMU uses an accelerometer, gyroscope and magnetometer to find and define a concrete coordinate system in 3D space.
    The IMU uses the NDOF fusion mode to to balance the ball on the platform. This lab consists of using the IMU chip and both PMDC motors to bring the platform 
    to level. A PID controller is used to neglect finger tapping on the corner of the platform and bring the platform to level with minimal fluxuation and oscillation. 
    The final PID values for the leveling of the platform are P = 4.5, I = 35, D = 0.20. 
    

   
    @author Jason Hu
    @author Ally Lai
    
    @copyright no associated license

    @date February 23, 2022
'''