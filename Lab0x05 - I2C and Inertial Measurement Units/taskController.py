'''!@file       taskController.py
    @brief      A generator to update attributes based on a closed loop controller
    @details    The generator updates the duty cycle share object from a value given by
                the ClosedLoop file based on inputs for the measured velocity, velocity setpoint,
                and desired proportional gain. This action is turned on and off by the w_Flag share
                object which controls whether the generator function is an a disabled or enabled state.
'''

from time import ticks_us, ticks_add, ticks_diff
import ClosedLoop

def taskControllerFcn(taskName, period, m_Flag, ref_vel, meas_vel, kp, duty_cycle, w_Flag):
    state = 0
    
    ##Creating ClosedLoop Object
    closed_loop = ClosedLoop.ClosedLoop()   
    ## Timing Block 
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
            ## End Timing Block
            
            ## Exit initial zero state
            if state == 0:
                state = 1
                
            ## c_Flag == Controller flag
           ## Disabled controller task 
            elif state == 1:
                ##User wants to run the ClosedLoop function
                if w_Flag.read() == True:
                    state = 2
                    
                    
            ## Enabled controller task        
            elif state == 2:
                if w_Flag.read() == False:
                    state = 1
                else:
                    DC = closed_loop.run(kp.read(), ref_vel.read(), meas_vel.read())
                    duty_cycle.write(DC)
                    
            yield(state)
        else:
            yield(None)
                
                
                ## User wants to change the KP value.
            
                    
                        
                        
                        
    