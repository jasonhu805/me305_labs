'''!@file       Final_Term_Project_Page.py
    @brief      A page documenting the usage, testing and tuning of the ME 305 Final Term project

    @page  Lab0xFF ME 305 Lab0xFF
 
    @section Lab0xFF Lab0xFF: Ball-Balancing Platform
    
@subsection Introduction

This project involves using the hardware that we have been familiarizing ourselves with all quarter to balance a ball on top of a platform. 
The hardware used include two PMDC motors, BNO055 IMU chip, Resistive Touchpanel, and STM32 Nucleo. The project is coded in Python and run using the Putty terminal window.

@subsection Files
  
The files used for this project are broken down into three different types. First is the driver files. These files directly interact with hardware
and usually return raw values from the hardware. Examples of driver files include the touchpanel driver and the motor driver. 
Second, we have the Task files. These files interact with the driver files to do any and all calculations. They do not interact with the hardware but instead interact with the 
raw values outputted by the driver files. Examples of Task files include TaskTouchPanel, TaskMotor, Taskcontroller and Taskuser. 
Lastly we have the main file, that runs each task based on the given frequency and order. In this main file we instantiate shares and flags that faciliate communication between all the files. 

@subsection Testing
   
To begin testing, we used a guess-and-check methodology to find the values of Kp, Ki, and Kd, then used intuition and data collection to change the values of
kp, ki, kd, alpha and beta. Throughout the tuning process, data and graphs were collected and plotted to gain insight on the inner workings of tuning. Data collected includes the X and Y position and 
velocity and the duty cycle values. We did this for both alpha-beta filtered and unfiltered calculations. Images of position, velocity and duty cycle versus time are shown below.
Multiple iterations of these graphs were made, and the values of Kp, Ki, Kd, alpha and beta were changed until the platform exhibited behavior that we felt was reasonable for this project. 
At the submission of this project, the variable values are as follows:

                    Inner Loop    Outer Loop
            Kp        7.565     |   .185
            Ki          0       |     0
            Kd        0.325     |   0.155
            Alpha             0.80
            Beta              0.51
    
@image html Lab0xFF_unfiltered.PNG "Unfiltered X-Y Data" width = 750
@image html Lab0xFF_Filtered.PNG "Filtered X-Y Data" width=750   

As seen in the images above, using alpha-beta filtering on the X and Y coordinates lets the position, velocity and duty cycle become smoother. This in turn lets the program and platform operate with less
"Jolty-ness". This Jolty-ness is caused by oscillations that come from the ball moving very fast and a significant amount of noise. 
Alpha-beta filtering causes the noisy spikes to smoothen out and the program to operate smoother. As well as this, using alpha-beta filtering results in less noisy motor operation and minimal belt slipping. 

Below is a video showing preliminary testing of the platform using basic guess-and-check tuning values. 

\htmlonly
<iframe width="560" height="315" src="https://www.youtube.com/embed/OPXzopaYsow" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly 
    
    
@image html Lab0xFF_STD.PNG "State Transition Diagram" width=1000
The image above shows the State Transition Diagram for the program as a whole. The entirety of the program is controlled by 'taskUser.py' which specifies what will be done and when. All the files run at
10000Hz. It is important not to have blocking code so all these states can run at the same time. When the user presses buttons, the file 'taskUser.py' sends out flags to other files to perform operations. 
An example of this is when the user hits the F button, taskUser sends out a flag to taskTouchpanel to start alpha-beta filtering the positions and velocities of the ball. 

@image html Lab0xFF_TD.PNG "Task Diagram" width=750
The Task Diagram shown above shows the routes that each shared variable takes. For example, when the user presses F for f_Flag and 'taskTouchpanel' sends the shares 'ball__coords' and 'ball_vel' 
to the task controller file.


@image html Lab0xFF_BD.PNG "Block Diagram" width=1500
The Block Diagram shown above shows the closed-loop feedback control that the program follows. 

The user menu is shown below. 
    
                    +-------------------------------------+
                    | Welcome to the Ball Balancing       |
                    | Platforms User Interface            |
                    |-------------------------------------|
                    | To use the interface, press:        |
                    | "P" to print Euler angles from the  |
                    |         IMU                         |
                    | "V" to print angular velocity from  |
                    |         the IMU                     |
                    | "U" to print calibration status     |
                    | "C" to write calibration coeffs     |
                    |         to IMU                      |
                    | "Q" to calibrate touch panel        |
                    | "F" to filter touch panel readings  |
                    | "m" to enter duty cycle for motor 1 |
                    | "M" to enter duty cycle for motor 2 |
                    | "T" to start testing interface      |
                    | "S" to stop data collection or test |
                    |      interface prematurely          |
                    | "H" to print this help menu         |
                    +-------------------------------------+
                    | Closed Loop Controller Functions:   |
                    |                                     |
                    | press "k" to enter new inner loop   |
                    |               gain values           |
                    | press "K" to enter new outer loop   |
                    |               gain values           |  
                    | press "W" to enable or disable      |
                    |           platform balancing        |
                    | press "J" to collect data for 15    |
                    |               seconds               |
                    +-------------------------------------+

@subsection Operation
The maximum time we were able to get the ball to balance was about 16 seconds. In order to do this, the ball must be placed in the middle of the platform with zero velocity. 
This is analogous with homogeneous initial conditions. The X and Y coordinates and zero and the X any Y velocities are zero. A video of the user interface and the ball balancer in action
are shown below. The platform also had an offset value that had to be found using a bubble level. This offset value is then hard-coded into the program to ensure that the platforms stays
mostly level.  

@image html Lab0xFF_bubble.PNG "Bubble Level" width=500

Ball Balancing Example

\htmlonly
<iframe width="560" height="315" src="https://www.youtube.com/embed/01COpgxKh2I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly

User Interface Example

\htmlonly
<iframe width="560" height="315" src="https://www.youtube.com/embed/JImQc8J4W1s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly
   
Closed Loop Controller Explination

\htmlonly
<iframe width="560" height="315" src="https://www.youtube.com/embed/3G0gw5G3gmM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly


@author Jason Hu
@author Ally Lai

@copyright no associated license

@date February 23, 2022
'''