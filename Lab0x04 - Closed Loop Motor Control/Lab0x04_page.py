'''!@file       Lab0x04.py
    @brief      A page providing documentation, plots and supporting material for ME 305 Lab0x04

    @page  lab0x04 ME 305 Lab0x04
 
    @section Lab0x04 Lab0x04: Closed Loop Control
    
    @subsection desc Description:
    
    Lab0x04 consists of closed-loop control functionality for the DC motors we work with in the lab. This closed-loop control 
	works by integrating user input and measured speed to calculate a duty cycle. The duty cycle directly sets the motor to a new speed. 
	The equation: 
	L = K_p * (w_reference - w_measured) 
	is iterated through for three seconds to produce the plots below. 
	The combined value of (w_reference - w_measured) is the calculation of velocity error. It is the difference between the users desired speed
	and the motors measured speed. This value is then multiplied by the users input K_p and a duty cycle is calculated. 
	The "sweet spot" is where the motor will settle down to after a tens of thousands of iterations through the equation. 
	This sweet spot is found when the velocity error reaches a point where the duty cycle calculation leads to the same velocity error. 
    
    @image html 2-23_STD.PNG "State Transition Diagram" width=1000
	

    @image html CLC_block_diagram.PNG "Block Diagram for Closed Loop Control" width=500
	

    @image html task_controller.PNG "Task Controller State Transition Diagram" width=500
	

    @image html task_diagram.PNG "Task Diagram" width=500
	
    After the closed loop controller was implimented, a tuning process had to take place. 
    In this tuning process, the value of proportional gain, Kp, is adjusted until the system
    exhibits behavior that we anticipate. 
    @image html lab0x04_kp_0_05.JPG "Kp set equal to 0.05" width=500
    This system exhibits good behavior, except the steady state error is too large, we will increase
    the proportional gain value to decrease it.
    
    @image html lab0x04_kp_0_25.JPG "Kp set equal to 0.25" width=500
    This system shows less steady state error, also, overshoot is introduced into the system.
    Along with overshoot, settling time is introduced. 
    The proportional gain will be increased again, this time, to decrease the settling time. 
    
    @image html lab0x04_kp_0_5.JPG "Kp set equal to 0.5" width=500
    The settling time has been decreased dramatically. The steady state error is also decreased with each
    iteration of increased Kp. 
    We will increase the gain again to see what happens to the system.
    
    @image html lab0x04_kp_0_60.JPG "Kp set equal to 0.6" width=500
    In this, the settling time has been increased. This is bad for the system as it will take longer for it to
    reach steady-state. 
    The proportional gain will be increased one more time.
    
    @image html lab0x04_kp_1.JPG "Kp set equal to 1" width=500
    This system exhibits steady-state error on the other side of the spectrum. The steady state gain value is
    higher than the setpoint. We also see significant oscillation.
    This system shows too high of a gain value. 
   
    @author Jason Hu
    @author Ally Lai
    
    @copyright no associated license

    @date February 23, 2022
'''