%% Given Constants
r_m = 60; %mm
l_r = 50; %mm
r_b = 10.5; %mm
r_g = 42; %mm
l_p = 110; %mm
r_p = 32.5; %mm
r_c = 50; %mm
m_b = 30; %grams
m_p = 400; %grams
I_p = 1.88*10^6; %g*mm^2
I_b = (2/5)*m_b*r_b^2;
b = 10; %mN*m*s/rad
g = 9.81; %m/s^2

% Symbolic Variables
syms x(t) theta(t) M_y 
assume(M_y, {'positive', 'real'})

% Differentiation
%% Position Vector (X)
r_ball = [r_c*sin(theta) + l_p*cos(theta)
    r_c*cos(theta) - l_p*sin(theta)];
%% Velocity Vector (X_dot)
v_ball = diff(r_ball, t);
%% Acceleration Vector
a_ball = diff(v_ball, t);

%Substitute Function
r_ball = subs(r_ball(0), [theta             diff(theta, t),    diff(theta,t,t), ...
                          x                 diff(x,t),         diff(x,t,t)], ...
                         [sym('theta')      sym('theta_dot')   sym('theta_ddot'), ...
                          sym('x')          sym('x_dot')       sym('x_ddot')]);

v_ball = subs(v_ball(0), [theta             diff(theta, t),    diff(theta,t,t), ...
                          x                 diff(x,t),         diff(x,t,t)], ...
                         [sym('theta')      sym('theta_dot')   sym('theta_ddot'), ...
                          sym('x')          sym('x_dot')       sym('x_ddot')]);

a_ball = subs(a_ball(0), [theta             diff(theta, t),    diff(theta,t,t), ...
                          x                 diff(x,t),         diff(x,t,t)], ...
                         [sym('theta')      sym('theta_dot')   sym('theta_ddot'), ...
                          sym('x')          sym('x_dot')       sym('x_ddot')]);

% Symbolic Assumptions
x = sym('x', 'real');
xd = sym('x_dot', 'real');
xdd = sym('x_ddot', 'real');
th = sym('theta', 'real');
thd = sym('theta_dot', 'real');
thdd = sym('theta_ddot', 'real');

%% M Matrix Elements
M_11 = (I_b/r_b) + m_b*(r_b + r_c);
M_12 = I_p + m_p*r_g^2 + I_b + m_b*((r_b + r_c)^2 + x^2);
M_21 = (I_b/r_b) + m_b*r_b;
M_22 = I_b + m_b*r_b*(r_b + r_c);
M = [M_11 M_12
    M_21, M_22];

%% F Matrix Elements
F_1 = M_y + (m_p*r_g + m_b*(r_b+r_c))*g*sin(th) + m_b*x*g*cos(theta) - 2*m_b*x*xd*thd;
F_2 = m_b*r_b*g*sin(th) + m_b*r_b*x*thd^2;
F = [F_1
    F_2];

%% Linearization
array = inv(M).*F
% 
% g_array = [ xd
%            thd
%            array(1)
%            array(2)];



