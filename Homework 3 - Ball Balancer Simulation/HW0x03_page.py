'''!@file       HW0x03.py
    @brief      A page providing documentation, plots and supporting material for ME 305 HW0x03

    @page HW0x03 ME 305 HW0x03
 
    @section HW0x03 HW0x03: Ball Balancing Simulation
    
    @subsection desc Description:
    
    Homework 0x03 consists of a MATLAB simulation of the ball balancing system built off of the hand calculations from Homework 0x02. 
    The derivations calculated in Homework0x02 include freebody diagrams and kinetic diagrams of both the platform and the ball. 
    FBD and KD images are included below. 
    The derivations found in Homework0x02 are left in state-variable form, meaning that the terms x, x_dot, theta and theta_dot are left as open variables. 
    
    
    @image html HW0x03_Diagram.PNG "Ball and Platform Diagram" width=500
    This image shows the ball and platform with their respective coordinate systems. 
    
    @image html HW0x02_FBD_KD.PNG "Freebody and Kinetic Diagrams for the Ball and Platform" width=500
    This image shows the FBD and KD for the ball and platform, along with their respective forces and kinetics.
    Note that the FBD shows static forces on the ball and platform such as gravity and torque.
    The KD shows kinetic forces that are represented as movement in the ball and platform. 
    
    
    @image html HW0x02_FBD_KD_BALL.PNG "Freebody and Kinetic Diagram for Ball Only" width=500
    This image shows the freebody diagram and kinetic diagram for the ball only. 
    Static force and Kinetic forces are separated. 
    
    @image html HW0x02_Derivations.PNG "Matrix Derivation" width=500
    This image shows the matrix derivation for the ball-balancing platform. 
    Note the state variables x_doubledot and theta_doubledot.
    These are entered into MATLAB and the state variables x, x_dot, theta, and theta_dot are calculated using matrix algebra.
    
   

    The derivations found in Homework0x02 are entered into MATLAB in matrix form.
    Matrix algebra is employed to find the Jacobian of state variables x, x-dot, theta and theta-dot. 
    x is the placeholder for relative position of the ball along the platform, its derivative, x-dot is the velocity of the ball realative to the platform. Theta is the angle of
    platform relative to the ground. Its derivative, theta-dot, is the angular velocity of the platform relative to the ground. 

    Below are four plots of two different situations. 
    In Situation 1, the ball is initially at rest on a level platform directly above the center of gravity of the platform. There 
    is no torque input from the motor (T_x = 0). The simulation is ran for 1 second.
    In Situation 2, the ball is initially at rest on a level platformoffset horizontally from the center of gravity of the platform 
    by 5 centimeters. There is no torque input from the motor and the simulation is ran for 0.4 seconds. 

    @htmlinclude HW0x03_MATLAB1.html

        

   
    @author Jason Hu
    
    @copyright no associated license

    @date February 19, 2022
'''