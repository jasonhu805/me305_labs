'''!@file       HW0x02_page.py
    @brief      A page providing documentation and supporting material for ME 305 homework 0x02

    @page HW0x02 ME 305 HW0x02
 
    @section HW0x02 HW0x02: Ball Balancing Model
    
    @subsection desc Description:
    
	Homework 0x02 consists of modeling the ball-balancing system using a dynamic analysis. 
    
	There are 7 pages associated with the calculation of the final matrix, which consists of coefficients of x double-dot and theta double-dot.
	These terms are the relative acceleration of the ball in the x direction and the angular acceleration of the platform under a motor torque, Tx.
	
    
    @image html HW0x02.1.jpg "" width=1000

    @image html HW0x02.2.jpg "" width=1000

    @image html HW0x02.3.jpg "" width=1000

    @image html HW0x02.4.jpg "" width=1000

    @image html HW0x02.5.jpg "" width=1000

    @image html HW0x02.6.jpg "" width=1000

    @image html HW0x02.7.jpg "" width=1000
    


        
   
    @author Jason Hu
    
    @copyright no associated license

    @date February 3, 2022
'''