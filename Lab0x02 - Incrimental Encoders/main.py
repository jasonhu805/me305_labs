'''!@file       main.py
    @brief      
    @details    
'''

import shares, taskUser, taskEncoder

## @brief A shared variable indicating whether the encoder position should be zeroed
z_Flag = shares.Share(False)
## @brief A shared variable indicating encoder position
position = shares.Share(False)
## @brief A shared variable indicating encoder delta
delta = shares.Share(False)

if __name__ == "__main__":
    '''!@brief              Constructs an empty queue of shared values
    '''
    ## @brief A list of tasks which includes one run of the user and encoder tasks for each cycle
    taskList = [taskUser.taskUserFcn      ('Task User', 10_000, z_Flag, position, delta),
                taskEncoder.taskEncoderFcn('Task Encoder', 10_000, z_Flag, position, delta)]
    
    #Generator for the user and encoder functions
    while True:
            try:            
                for task in taskList:
                    next(task)    
            except KeyboardInterrupt:
                break
    print("Program Terminated")