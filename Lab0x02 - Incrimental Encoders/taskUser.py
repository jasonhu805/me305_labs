'''!@file       taskUser.py
    @brief      A generator to implement UI tasks as part of an FSM.
    @details    During each iteration the generator initiates actions based 
                on the entered sampling period and user inputs.
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP
import array

def taskUserFcn(taskName, period, z_Flag, position, delta):
    '''!@brief              A generator function which returns a value for state related to a UI task
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param z_Flag       A Share object which is used to indicate that an 
                            action related to entering the "z" key should be performed
        @param position     A Share object respresenting the position attribute of the encoder
        @param delta        A Share object respresenting the delta attribute of the encoder
    '''

    state = 0
    count = 0 
    time = 0.00
    DatArray = array.array('l',[position.read()])
    timeArray = array.array('f', [0])

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    ## @brief A (virtual) serial port object used for getting characters cooperatively.
    serport = USB_VCP()
    
    print('+------------------------------------+')
    print('| Welcome to the Encoder Interface   |')
    print('|------------------------------------|')
    print('| To use the interface, press:       |')
    print('| "Z" to zero the position           |')
    print('| "P" to get position                |')
    print('| "D" to get the delta value         |')
    print('| "G" to collect data for 30 seconds |')
    print('| "S" to stop data collection        |')
    print('|      prematurely                   |')
    print('| "H" to print this help menu        |')
    print('+------------------------------------+')    
      
    while True:
        current_time = ticks_us()
    
   
        if ticks_diff(current_time,next_time) >= 0:
        

            next_time = ticks_add(next_time, period)
            

            if state == 0:
          
                state = 1  
   
            elif state == 1:
 
                if serport.any():
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'z','Z'}:
                        print('Typed Z for Zero')
                        state = 2
                        z_Flag.write(True)
                    elif charIn in {'p','P'}:
                        print('Typed P for Position')
                        print(position.read())
                    elif charIn in {'d','D'}:
                        print('Typed D for Delta')
                        print(delta.read())
                    elif charIn in {'g','G'}:
                        state = 3
                        print('Data Collection Beginning')
                    elif charIn in {'s','S'}:
                            print('Data is not being collected, press G to start data collection')
                    elif charIn in {'h','H'}:
                        print('+------------------------------------+')
                        print('| Welcome to the Encoder Interface   |')
                        print('|------------------------------------|')
                        print('| To use the interface, press:       |')
                        print('| "Z" to zero the position           |')
                        print('| "P" to get position                |')
                        print('| "D" to get the delta value         |')
                        print('| "G" to collect data for 30 seconds |')
                        print('| "S" to stop data collection        |')
                        print('|      prematurely                   |')
                        print('| "H" to print this help menu        |')
                        print('+------------------------------------+')  
                        
                        
                    
            # for input z zero encoder position
            elif state == 2:
                
                if z_Flag.read() == False:
                    state = 1 # transition to state 0
                    
            elif state == 3:
                if count <= 3001:
                    
                    DatArray.append(position.read())
                    
                    timeArray.append(time)
                    
                    time += (1/100)
                    
                    count += 1
                    
                    
                    if serport.any() == True:
                        if serport.read(1).decode() in {'s','S'}:
                            state = 4
                            
                else:
                    state = 4
                    
            elif state == 4:
                print("Data Collection Stopped")
                print('----------Start Data----------')
                for i,n in zip(DatArray, timeArray):
                    print(str(i) + ', ' + "{:.2f}".format(n))
                print('----------End Data----------')    
                DatArray = array.array('l',[position.read()])
                timeArray = array.array('f', [0])
                count = 0
                time = 0
                state = 1
                        
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield state
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None
