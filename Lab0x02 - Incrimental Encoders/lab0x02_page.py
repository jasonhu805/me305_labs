'''!@file       lab0x02_page.py
    @brief      A page providing documentation and supporting material for ME 305 Lab0x02

    @page lab0x02 ME 305 Lab0x02
 
    @section Lab0x02 Lab0x02: Encoder
    
    @subsection desc Description:
    
    Lab0x02 consists of a user interface which allows interaction with a simple encoder. The interface
    prompts the user to enter different commands using specific key inputs. The commands include zeroing the
    relative position of the encoder, getting the current position or delta value, (change in position), collecting
    position data for up to 30 seconds, and stopping data collection. 
    
    Below is a reference plot of 30 seconds of encoder position data recorded using this lab as well as a task diagram 
    and finite state diagrams for each task to illustrate the structure of the code. A link to a repository containing 
    the source code is also listed below. Documentation of each file created for this lab can be found under the "Files"
    tab of this website.

    The following finite state diagrams outline the functionality of the user and encoder task files
    which interact with the user interface and encoder inputs/outputs respectively.
    
    @image html lab0x02_FSM1.jpg "User Task Finite State Diagram" width=500
    
    @image html lab0x02_FSM2.jpg "Encoder Task Finite State Diagram" width=500
    
    The following task diagram shows how data is shared between the user and encoder tasks:
        
    @image html lab0x02_TD.jpg "Lab0x02 Task Diagram" width=400
    
    The following graph shows an example of what the output for 30 seconds of encoder data might look
    like. As visible in the graph, the position of the encoder is updates in discrete increments which 
    are visible to the eye at a sampling frequency of 100 Hz.
    
    @image html lab0x02_exData.jpg "Example Encoder Data" width=400
    
    Bitbucket Repository Link: https://bitbucket.org/alai09/alai09.bitbucket.io/src/main/Source%20Code/Lab0x02/
        
    @author Ally Lai
   
    @author Jason Hu
    
    @copyright no associated license

    @date February 3, 2022
'''