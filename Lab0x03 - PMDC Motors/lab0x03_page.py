'''!@file       Lab0x03.py
    @brief      A page providing documentation, plots and supporting material for ME 305 Lab0x03

    @page  lab0x03 ME 305 Lab0x03
 
    @section Lab0x03 Lab0x03: DC Motor Control Control
    
   
    
    Lab 0x03 builds off the user interface for lab 0x02. It utilizes the DRV8847 motor driver chip to
    control both DC motors at the same time. The user interface includes setting the duty cycles for
    each induvidual motor. The interface also detects and clears faults that may occur due to large changes 
    in duty cycle. A testing interface is added to control the motor and record velocity as duty cycle changes.
    The main control features built into lab0x03 is built off of the encoder drivers that lab0x02 implimented. 
    The original encoder user interface remains the same, with added features to incorperate the driver. 
    
    Below is a task diagram showing how data is shared between the user, encoder and motor tasks. 
    
    
    @image html Lab0x03_taskdiagram.JPG "Task Diagram" width=500
	
    
    Next is a state transition diagram for lab0x03. It shows how the user, motor and encoder tasks transition between states
    based on user input and general behavior. 
    
    @image html Lab0x03STD.JPG "State Transition Diagram" width=500
	

    Next, the state transition diagram for the encoder task shows how the general flow of information though
    the file taskEncoder.py happens. 

    @image html Lab0x02_FSM2.JPG "State Transition Diagram" width=500
	

    Next, the motor task state transition diagram shows the general flow of information through the file
    taskMotor.py. This time, it calls functions from the DRV8847.py file. 
    
    @image html Lab0x03DRV8847.JPG "State Transition Diagram" width=500
	

    The following graphs shows an example of what a 30 second output from the testing interface may look like.
    It is noted that the sampling frequency of the encoder is 100Hz. 

    @image html lab0x03_30sec.png "Example Encoder Data" width=500
    
    Finally, the last graph shows the relationship between duty cycle and motor velocity. It is noted that
    the curve fit is linear, meaning that the relationship between duty cycle and velocity do not change 
    with motor speed, it is a proportional relationship. 
    
    @image html lab0x03_duty_cycle.png "Duty Cycle vs. Velocity Plot" width=500
    
    
        

   
    @author Jason Hu
    @author Ally Lai
    
    @copyright no associated license

    @date February 28, 2022
'''