'''!@file       encoder.py
    @brief      Object which allows interfacing with a simple encoder
    @details    Implements an interface for reading data from an encoder, specifically
                the relative angular position of the encoder. Also allows the relative
                position to be set back to zero.
'''

import pyb

class Encoder:
    '''!@brief      The encoder object class that creates encoder objects. 
        @details    Each motor has an encoder object that detects ticks, which are 
                    related to rotation, and calculates a delta, or change value from
                    it. This delta is then used to calculate speed later down the line. 
    '''
    
    def __init__(self, port, timnum):
        '''!@brief          Constructs an encoder variable
            @details        Defines an encoder based on a specified port and timer number.
                            The encoder object is assigned to a channel based on the port
                            and timer number. The initial position of the encoder is defined
                            as zero and an overflow value is defined to be accessed by the 
                            other functions.
            @param port     An input value which allows the user to select
                            which port the encoder they're accessing to connected to.
            @param timnum   An input value which specifies the timer used to 
                            which specifying a channel to interface with the encoder 
                            associated with the intiatilzed encoder object.
        '''
        ## @brief The position of the encoder in "ticks"
        self.position = 0
        
        ## @brief Used to store the last position of the encoder in "ticks"
        self.lastposition = 0
        
        ## @brief The difference between the current and most recent encoder positions recorded
        self.delta = 0
        
        ## @brief The autoreload value at which the position output of the microcontroller sets back to zero
        self.AR = 65535
        
        ## @brief The threshold value of delta for which it can be assumed that
        #         between the current and last positions the output "overflowed."
        self.OFval = (self.AR+1)/2

        if port == 'B':
            pin1 = pyb.Pin (pyb.Pin.cpu.B6, pyb.Pin.OUT_PP)
            pin2 = pyb.Pin (pyb.Pin.cpu.B7, pyb.Pin.OUT_PP)
        
        elif port == 'C':
            pin1 = pyb.Pin (pyb.Pin.cpu.C6, pyb.Pin.OUT_PP)
            pin2 = pyb.Pin (pyb.Pin.cpu.C7, pyb.Pin.OUT_PP)
    
        ## @brief The timer object associated with the encoder
        self.timX = pyb.Timer(timnum, prescaler = 0, period = 65535)

        ## @brief The channel object associated with the first pin encoder connection
        self.t4ch1 = self.timX.channel(1, pyb.Timer.ENC_A, pin=pin1)
        
        ## @brief The channel object associated with the seconds pin encoder connection
        self.t4ch2 = self.timX.channel(2, pyb.Timer.ENC_B, pin=pin2)
        
        self.position = self.timX.counter()
  
    
    
    def update(self):
        '''!@brief      Updates the position attribute based on delta and elapsed time
            @details    Updates the position of the motor by finding the change in ticks
                        from the last update call. It then takes this change and adds it
                        to the last known position. It also includes 'if' statements to
                        handle overflow.
        '''
        self.delta = self.timX.counter() - self.lastposition
        self.lastposition = self.timX.counter()
        
        if self.delta > self.OFval:  
            self.delta -= 65535
            self.position += self.delta
        
        elif self.delta < -self.OFval:
            self.delta += 65535
            self.position += self.delta
            
        else: 
            self.position += self.delta
            
        
    def get_position(self):
        '''!@brief  Access the value of the position attribute
           @return  The position attribute of the encoder
        '''
        return self.position
    
    
    def zero(self):
        '''!@brief  Sets the position attribute of the encoder back to zero  
        '''
        self.position = 0
    
        
        
    def get_delta(self):
        '''!@brief   Access the value of the delta attribute
            @return  The delta attribute of the encoder
        '''
        return self.delta
    
    
