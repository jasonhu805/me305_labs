'''!@file                mainpage.py
    @brief               Brief doc for mainpage.py
    @details             Detailed doc for mainpage.py 
    
    @mainpage 
    
    @image html Selfie.JPG "Jason Hu" width=250
    
    
    @section sec_intro   Introduction
    Welcome to Jason Hu's ME 305 Online Repository. If you are viewing this, you are either a grader or Charlie. This repository serves to organize and present
    my progress throughout the course of ME 305 - Intro to Mechatronics at California Polytechnic State University San Luis Obispo, CA 
    Documentation for each project completed in this course can be found under the 'Files' tab above.
    
    
    @section sec_term   Final Term Project
    The final term project involves using PID controllers to balance a ball on top of a platform. 
    A picture of the platform and the BNO055 chip are shown below. More resources on this project can be found in the 'Related Pages' tab under 'Lab0xFF'.
    
    
    @image html platform.jpg "Ball Balancing Platform" width=500
    @image html BNO055.jpg "BNO055 Chip" width=500
    
    
    @section sec_mot     Motor Driver
    Some information about the motor driver with links.
    Please see motor.Motor for details.
    
    @section sec_enc     Encoder Driver
    Some information about the encoder driver with links. 
    Please see encoder.Encoder for details.
    
    @section sec_files   Online Repository
    The online Bitbucket repositiory can be found at: 
     https://bitbucket.org/jasonhu805/me305_labs/src/master/
    
    @author              Jason Hu
    
    @copyright           These documents are not licensed 
    
    @date                March 13, 2022
'''