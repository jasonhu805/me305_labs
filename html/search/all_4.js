var searchData=
[
  ['dc_0',['DC',['../class_closed_loop_1_1_closed_loop.html#a331d259464e17a3e15f79e86ac4319ea',1,'ClosedLoop::ClosedLoop']]],
  ['delta_1',['delta',['../classencoder_1_1_encoder.html#ad017c0a5f382fe0dac6ed8920ce90635',1,'encoder.Encoder.delta()'],['../_lab0x02_01-_01_incrimental_01_encoders_2main_8py.html#a01cf4e8a64081698689afb33f0fc217d',1,'main.delta()']]],
  ['disable_2',['disable',['../class_d_r_v8847_1_1_d_r_v8847.html#acd9dbef9212b3014eab18a57a6e0f13a',1,'DRV8847.DRV8847.disable(self)'],['../class_d_r_v8847_1_1_d_r_v8847.html#acd9dbef9212b3014eab18a57a6e0f13a',1,'DRV8847.DRV8847.disable(self)'],['../class_d_r_v8847_1_1_d_r_v8847.html#acd9dbef9212b3014eab18a57a6e0f13a',1,'DRV8847.DRV8847.disable(self)']]],
  ['drv8847_3',['DRV8847',['../class_d_r_v8847_1_1_d_r_v8847.html',1,'DRV8847']]],
  ['drv8847_2epy_4',['DRV8847.py',['../_lab0x03_01-_01_p_m_d_c_01_motors_2_d_r_v8847_8py.html',1,'(Global Namespace)'],['../_lab0x04_01-_01_closed_01_loop_01_motor_01_control_2_d_r_v8847_8py.html',1,'(Global Namespace)'],['../_lab0x05_01-_01_i2_c_01and_01_inertial_01_measurement_01_units_2_d_r_v8847_8py.html',1,'(Global Namespace)']]],
  ['duty_5fcycle_5',['duty_cycle',['../_lab0x03_01-_01_p_m_d_c_01_motors_2main_8py.html#a1e9dbc680a008ef10f4509c2d691383c',1,'main']]],
  ['duty_5fcycle1_6',['duty_cycle1',['../_lab0x_f_f_01-_01_term_01_project_2main_8py.html#a593eaa433f1a91f3fafbe0b4ca034606',1,'main']]],
  ['duty_5fcycle2_7',['duty_cycle2',['../_lab0x_f_f_01-_01_term_01_project_2main_8py.html#afe7767c26f84049c01a9c244ebdd2980',1,'main']]],
  ['duty_5fmax_8',['duty_max',['../class_closed_loop_1_1_closed_loop.html#a3af64ca355e93fae0dc8998b2b27b6ad',1,'ClosedLoop::ClosedLoop']]],
  ['duty_5fmin_9',['duty_min',['../class_closed_loop_1_1_closed_loop.html#a90b70f2a6b12accfca10b779059410ff',1,'ClosedLoop::ClosedLoop']]]
];
