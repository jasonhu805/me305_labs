var searchData=
[
  ['set_5fcall_0',['set_call',['../_lab0x_f_f_01-_01_term_01_project_2main_8py.html#a8a776ca715032cbcdeda723427f07c03',1,'main']]],
  ['set_5fduty_1',['set_duty',['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)']]],
  ['set_5fkd_2',['set_kd',['../class_closed_loop_1_1_closed_loop.html#a8ae1d4b7c62492426ab097563c97cf79',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fki_3',['set_ki',['../class_closed_loop_1_1_closed_loop.html#aadb0c5c408cdfeb92dbd270bf651dfdc',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fkp_4',['set_kp',['../class_closed_loop_1_1_closed_loop.html#ad01830e99c4aedf96217fafa6cd94909',1,'ClosedLoop::ClosedLoop']]],
  ['share_5',['Share',['../classshares_1_1_share.html',1,'shares']]],
  ['shares_2epy_6',['shares.py',['../_lab0x02_01-_01_incrimental_01_encoders_2shares_8py.html',1,'(Global Namespace)'],['../_lab0x03_01-_01_p_m_d_c_01_motors_2shares_8py.html',1,'(Global Namespace)'],['../_lab0x04_01-_01_closed_01_loop_01_motor_01_control_2shares_8py.html',1,'(Global Namespace)'],['../_lab0x05_01-_01_i2_c_01and_01_inertial_01_measurement_01_units_2shares_8py.html',1,'(Global Namespace)'],['../_lab0x_f_f_01-_01_term_01_project_2shares_8py.html',1,'(Global Namespace)']]],
  ['state_7',['state',['../task_b_n_o055_8py.html#a575e65a5773a6ff2a2112ea3e237578c',1,'taskBNO055']]],
  ['sum_8',['sum',['../class_closed_loop_1_1_closed_loop.html#a431f730aa8d7de4812c81901a17fd745',1,'ClosedLoop::ClosedLoop']]]
];
